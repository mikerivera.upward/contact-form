import enableTextField from './components/enableTextField';
import forEachPolyfill from './components/forEachPolyfill';
import searchPostalCode from './components/searchPostalCode';

document.addEventListener(
  'DOMContentLoaded',
  () => {
    forEachPolyfill()
    searchPostalCode()
    enableTextField()
  },
  false
)
