/**
* Enable input field when checkbox is checked for checkup type
*/

const enableTextField = () => {
  const checkbox4 = document.querySelector('.js-enable-input-for-4') as HTMLInputElement;
  const checkbox5 = document.querySelector('.js-enable-input-for-5') as HTMLInputElement;
  const input4 = document.querySelector('.js-enable-input-cb4') as HTMLInputElement;
  const input5 = document.querySelector('.js-enable-input-cb5') as HTMLInputElement;

  if (checkbox4) {
    checkbox4.onchange = () => {
      if (checkbox4.checked == false) {
        input4.disabled = true;
        input4.value = '';
      } else {
        input4.disabled = false;
      }
    }
  }

  if (checkbox5) {
    checkbox5.onchange = () => {
      if (checkbox5.checked == false) {
        input5.disabled = true;
        input5.value = '';
      } else {
        input5.disabled = false;
      }
    }
  }
}

export default enableTextField
