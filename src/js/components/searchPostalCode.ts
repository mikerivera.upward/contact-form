/**
* Search Postal Code from https://github.com/ajaxzip3/ajaxzip3.github.io
*/

const searchPostalCode = () => {
  const postalCodeInput = document.querySelector('.js-get-postal-code');
  const searchButton = document.querySelector('.js-search-postal-code');

  searchButton?.addEventListener('click', function() {
    //@ts-ignore
    AjaxZip3.zip2addr(postalCodeInput, '', 'address', 'address')
  })
}

export default searchPostalCode