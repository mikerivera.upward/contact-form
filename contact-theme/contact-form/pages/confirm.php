<?php  /* Template Name: Confirm */
get_header() ?>

<section class="banner"> <img class="banner__hero-image" src="<?php echo get_template_directory_uri() ?>/img/banner-img.jpg" alt="訪問診療">
    <div class="banner__hero-text-container">
      <h1 class="banner__hero-text">訪問診療</h1>
    </div>
  </section>
  <div class="website-container">
    <div class="breadcrumb">
      <ul class="breadcrumb__list">
        <li class="breadcrumb__item"><a class="breadcrumb__link" href="<?php echo get_home_url() ?>/top">トップ</a></li><span
          class="breadcrumb__arrow"><img src="<?php echo get_template_directory_uri() ?>/img/breadcrumb-arrow.png" alt="breadcrumb arrow"></span>
        <li class="breadcrumb__item breadcrumb__item--current"><span class="breadcrumb__link">訪問診療</span></li>
      </ul>
    </div>
  </div>
  <div class="website-container">
    <ul class="form-step">
      <li class="form-step__item">必要事項の入力</li>
      <li class="form-step__item form-step__item--current">入力内容の確認</li>
      <li class="form-step__item">送信完了</li>
    </ul>

    <?php
      echo do_shortcode('[mwform_formkey key="18"]')
    ?>

  </div>

<?php get_footer() ?>