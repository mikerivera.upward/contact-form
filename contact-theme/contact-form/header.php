<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=11">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
  <title>トップ</title>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css">
  <?php wp_head() ?>
</head>

<body>
  <header>
    <picture>
      <source media="(max-width: 767px) and (min-resolution: 2dppx)" srcset="<?php echo get_template_directory_uri() ?>/img/header-sp.jpg">
      <source media="(min-width: 800px) and (min-resolution: 2dppx)" srcset="<?php echo get_template_directory_uri() ?>/img//header.jpg">
      <source media="(min-width: 768px) and (min-resolution: 1dppx)" srcset="<?php echo get_template_directory_uri() ?>/img//header.jpg"><img class="header" src="<?php echo get_template_directory_uri() ?>/img/header-sp.jpg" alt="">
    </picture>
  </header>