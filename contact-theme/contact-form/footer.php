<footer><img class="footer" src="<?php echo get_template_directory_uri() ?>/img/footer.png" alt=""></footer>
  <script src="https://cdn.jsdelivr.net/npm/smoothscroll-polyfill@0.4.4/dist/smoothscroll.min.js"></script>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="utf-8"></script>
  <script src="<?php echo get_template_directory_uri() ?>/js\index.js" type="module"></script>
  <script>!function (e) { var t, a = { kitId: "lcr5yuz", scriptTimeout: 3e3, async: !0 }, c = e.documentElement, i = setTimeout((function () { c.className = c.className.replace(/\bwf-loading\b/g, "") + " wf-inactive" }), a.scriptTimeout), n = e.createElement("script"), s = !1, o = e.getElementsByTagName("script")[0]; c.className += " wf-loading", n.src = "https://use.typekit.net/" + a.kitId + ".js", n.async = !0, n.onload = n.onreadystatechange = function () { if (t = this.readyState, !(s || t && "complete" != t && "loaded" != t)) { s = !0, clearTimeout(i); try { Typekit.load(a) } catch (e) { } } }, o.parentNode.insertBefore(n, o) }(document);</script>
  <?php wp_footer() ?>
</body>

</html>